[![Build Status](https://travis-ci.org/keboola/juicer.svg?branch=master)](https://travis-ci.org/keboola/juicer) [![Code Climate](https://codeclimate.com/github/keboola/juicer/badges/gpa.svg)](https://codeclimate.com/github/keboola/juicer) [![Test Coverage](https://codeclimate.com/github/keboola/juicer/badges/coverage.svg)](https://codeclimate.com/github/keboola/juicer/coverage)


## Scrolling responses
- Using one of Keboola/Juicer/Pagination classes
- Or implement ScrollerInterface, to create a class returning Keboola\Juicer\Client\RequestInterface
    for first and subsequent requests
